export function useFormatDate(dateString) {
  // Ambil bagian tahun, bulan, dan hari dari string
  const year = dateString.substring(0, 4)
  const month = dateString.substring(4, 6) - 1 // Bulan di JavaScript Date dimulai dari 0
  const day = dateString.substring(6, 8)

  // Buat objek Date
  const date = new Date(year, month, day)

  // Format tanggal
  const options = { day: 'numeric', month: 'short', year: 'numeric' }
  return date.toLocaleDateString('en-EN', options)
}
