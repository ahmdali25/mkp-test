const products = [
  {
    id: 1,
    name: "Laptop",
    price: 1200,
    category: "Electronics",
  },
  {
    id: 2,
    name: "Smartphone",
    price: 800,
    category: "Electronics",
  },
  {
    id: 3,
    name: "Coffee Maker",
    price: 50,
    category: "Home Appliances",
  },
  {
    id: 4,
    name: "Running Shoes",
    price: 80,
    category: "Fashion",
  },
  {
    id: 5,
    name: "Headphones",
    price: 100,
    category: "Electronics",
  },
  {
    id: 6,
    name: "Digital Camera",
    price: 500,
    category: "Electronics",
  },
  {
    id: 7,
    name: "Toaster",
    price: 30,
    category: "Home Appliances",
  },
  {
    id: 8,
    name: "Backpack",
    price: 40,
    category: "Fashion",
  },
  {
    id: 9,
    name: "4K Smart TV",
    price: 800,
    category: "Electronics",
  },
  {
    id: 10,
    name: "Microwave Oven",
    price: 70,
    category: "Home Appliances",
  },
  {
    id: 11,
    name: "Gaming Console",
    price: 400,
    category: "Electronics",
  },
  {
    id: 12,
    name: "Leather Jacket",
    price: 120,
    category: "Fashion",
  },
  {
    id: 13,
    name: "Wireless Mouse",
    price: 20,
    category: "Electronics",
  },
  {
    id: 14,
    name: "Blender",
    price: 60,
    category: "Home Appliances",
  },
  {
    id: 15,
    name: "Wristwatch",
    price: 60,
    category: "Fashion",
  },
  {
    id: 16,
    name: "Desk Lamp",
    price: 25,
    category: "Home Appliances",
  },
];

// 1. Total harga semua produk
console.log("Jawaban Nomor 1");
let productPriceTotal = 0;
const calculateProductsPriceTotal = () => {
  productPriceTotal = products.reduce(
    (total, product) => total + parseInt(product.price),
    0
  );
  return productPriceTotal;
};
console.log(calculateProductsPriceTotal());

// 2. Array baru dengan produk yang harganya diatas 100
console.log("Jawaban Nomor 2");
const filteredArr = products.filter((product) => {
  return product.price > 100;
});
console.log(filteredArr);

// 3. Rata-rata harga produk
console.log("Jawaban Nomor 3");
const averageProductPrice = productPriceTotal / products.length;
console.log(averageProductPrice);

// 4. Urutan harga produk dari termurah hingga termahal
console.log("Jawaban Nomor 4");
const sortedProductPrice = products.sort((a, b) => a.price - b.price);
console.log(sortedProductPrice);

// 5. Total produk setiap kategori
console.log("Jawaban Nomor 5");
const totalProductsByCategory = products.reduce((acc, product) => {
  if (acc[product.category]) {
    // Jika kategori sudah ada dalam objek, tambahkan jumlah produk
    acc[product.category] += 1;
  } else {
    // Jika kategori belum ada dalam objek, inisialisasi dengan jumlah 1
    acc[product.category] = 1;
  }
  return acc;
}, {});
console.log(totalProductsByCategory);

// 6
console.log("Jawaban Nomor 6");
const changeProcuctName = () => {
  // Membuat salinan dari array products
  const arrProducts = products.map((product) => ({ ...product }));

  // Memodifikasi nama produk berdasarkan harga
  arrProducts.forEach((product) => {
    if (product.price > 200) {
      product.name = "Produk Premium";
    } else if (product.price < 50) {
      product.name = "Produk Hemat";
    }
  });

  return arrProducts;
};
console.log(changeProcuctName());

// 7
console.log("Jawaban Nomor 7");
const findProduct = (productInput) => {
  console.log("List Produk");
  console.log(products);
  const product = products.find((product) => product.name === productInput);

  if (product) {
    return product;
  } else {
    return "Produk tidak ditemukan";
  }
};

console.log(findProduct("Laptop"));
