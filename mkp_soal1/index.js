let price = 0;
let priceTotal = 0;
let addedPrice = 0;
let selectedProduct = "";
let selectedSize = "medium";

// array list of pizzas
const pizzas = [
  {
    name: "Pizza 1",
    toppings: ["avocado", "broccoli", "onions", "zucchini", "tuna", "ham"],
  },
  {
    name: "Pizza 2",
    toppings: [
      "broccoli",
      "onions",
      "zucchini",
      "lobster",
      "oyster",
      "salmon",
      "bacon",
      "ham",
    ],
  },
  {
    name: "Pizza 3",
    toppings: [
      "broccoli",
      "onions",
      "zucchini",
      "tuna",
      "bacon",
      "duck",
      "ham",
      "sausage",
    ],
  },
];

// array list of toppings
const toppings = [
  { label: "Avocado", value: "avocado", price: "1" },
  { label: "Broccoli", value: "broccoli", price: "1" },
  { label: "Onions", value: "onions", price: "1" },
  { label: "Zucchini", value: "zucchini", price: "1" },
  { label: "Lobster", value: "lobster", price: "2" },
  { label: "Oyster", value: "oyster", price: "2" },
  { label: "Salmon", value: "salmon", price: "2" },
  { label: "Tuna", value: "tuna", price: "2" },
  { label: "Bacon", value: "bacon", price: "3" },
  { label: "Duck", value: "duck", price: "3" },
  { label: "Ham", value: "ham", price: "3" },
  { label: "Sausage", value: "sausage", price: "3" },
];

function createCheckboxes() {
  const sectionCheckbox = document.getElementById("sectionCheckbox");

  toppings.forEach((item) => {
    const div = document.createElement("div");
    div.classList.add("toppings__radio");

    // create checkbox element
    const checkbox = document.createElement("input");
    checkbox.type = "checkbox";
    checkbox.value = item.value;
    checkbox.name = "option";
    checkbox.className = "item-checkbox";
    checkbox.id = `option-${item.value}`;

    // create label element
    const label = document.createElement("label");
    label.htmlFor = `option-${item.value}`;
    label.textContent = item.label;

    // add checkbox and label to div
    div.appendChild(checkbox);
    div.appendChild(label);
    div.appendChild(document.createElement("br"));

    sectionCheckbox.appendChild(div);

    // handle event listener for every input checkbox
    checkbox.addEventListener("click", function () {
      if (checkbox.checked) {
        addedPrice = Number(addedPrice) + Number(item.price);
      }

      priceTotal = Number(price) + Number(addedPrice);
      setPrice();
    });
  });
}

function setDefaultSize() {
  const radioElement = document.getElementById(`size-${selectedSize}`);
  if (radioElement) {
    radioElement.checked = true;
  } else {
    console.error(`Element with id size-${selectedSize} not found.`);
  }
}

function adjustPriceForSmall() {
  priceTotal = Number(price) + Number(addedPrice) - 1;
}

function adjustPriceForLarge() {
  priceTotal = Number(price) + Number(addedPrice) + 2;
}

function handleSelectProduct() {
  // reset added topping price if change pizza
  addedPrice = 0;

  if (document.getElementById("pizza1").checked === true) {
    price = document.getElementById("pizza1").value;
    selectedProduct = "Pizza 1";
    checkToppings(
      pizzas.find((item) => {
        return item.name == "Pizza 1";
      })
    );
  } else if (document.getElementById("pizza2").checked === true) {
    price = document.getElementById("pizza2").value;
    selectedProduct = "Pizza 2";
    checkToppings(
      pizzas.find((item) => {
        return item.name == "Pizza 2";
      })
    );
  } else if (document.getElementById("pizza3").checked === true) {
    price = document.getElementById("pizza3").value;
    selectedProduct = "Pizza 3";
    checkToppings(
      pizzas.find((item) => {
        return item.name == "Pizza 3";
      })
    );
  }

  priceTotal = price;

  if (selectedSize === "small") {
    adjustPriceForSmall();
  }

  if (selectedSize === "large") {
    adjustPriceForLarge();
  }

  setPrice();
}

function handleSelectSize() {
  priceTotal = price;

  if (selectedProduct) {
    if (document.getElementById("size-small").checked === true) {
      selectedSize = "small";
      adjustPriceForSmall();
    }

    if (document.getElementById("size-large").checked === true) {
      selectedSize = "large";
      adjustPriceForLarge();
    }
  }

  setPrice();
}

function checkToppings(pizza) {
  const topping = pizza.toppings;

  toppings.forEach((item) => {
    const input = document.getElementById(`option-${item.value}`);
    if (input) {
      input.disabled = false;
      input.checked = false;
    }
  });

  const filteredToppings = toppings.filter(
    (item) => !topping.includes(item.value)
  );

  filteredToppings.forEach((item) => {
    const input = document.getElementById(`option-${item.value}`);
    input.disabled = true;
  });
}

function setPrice() {
  document.getElementById("selectedPrice").textContent = `$${priceTotal}`;
}

document.addEventListener("DOMContentLoaded", function () {
  setPrice();
  setDefaultSize();
  createCheckboxes();
});
